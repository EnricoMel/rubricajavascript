// Programma per la gestione di una rubrica telefonica
// Crea un oggetto rubrica con la proprietà contatti che sarà un array di oggetti, ovvero i singoli contatti X
// I contatti dovranno avere almeno due proprietà: nome e telefono
// Implementare i metodi dell'oggetto per le seguenti operazioni:

// - Visualizzazione dell'intera lista contatti X
// - Ricerca passando il nome e restituendo il singolo contatto X
// - Cancellazione di un contatto passando in input il nome del contatto X
// - Inserimento di un nuovo contatto X
// - Modifica di un contatto passando in input il nome del contatto

// Creo manualmente l'oggetto rubrica
// Possiamo crearlo noi o averlo preso da un sistema esterno

// creo una variabile globale per gestirmi l'aggiunta di un contatto e aggiungerlo solo se la variabile è true, se è true aggiungerà il contatto visivamente (la card) solo se quando saranno visibili tutti
var showContatti = false; // la imposto su false

setTimeout(function () {
  console.log("Welcome");
}, 2000);

// setInterval(function() {
//     console.log('Rilancio la funzione ogni 3 secondi');
// }, 3000)

let rubrica = {
  contatti: [
    { nome: "Enrico", telefono: "+39 568 456 5698" },
    { nome: "Lucilla", telefono: "+39 458 742 6321" },
    { nome: "Marco", telefono: "+39 081 555 3654" },
    { nome: "Giuseppe", telefono: "+39 081 431 7258" },
    { nome: "Carmine", telefono: "+39 456 236 7895" },
    { nome: "Barbara", telefono: "+39 082 456 8788" },
    { nome: "Mario", telefono: "+39 083 456 7733" },
    { nome: "Gino", telefono: "+39 083 871 4466" },
    { nome: "Lucia", telefono: "+39 776 456 25 98" },
    { nome: "Camilla", telefono: "+39 879 456 3321" },
    { nome: "Fabio", telefono: "+39 879 456 3421" },
    { nome: "Pietro", telefono: "+39 879 456 3521" },
    { nome: "Pasquale", telefono: "+39 879 456 6321" },
    { nome: "Vincenzo", telefono: "+39 879 456 3391" },
    { nome: "Roberta", telefono: "+39 879 456 3621" },
    { nome: "Armando", telefono: "+39 879 456 3320" },
    { nome: "Nicola", telefono: "+39 878 456 3221" },
    { nome: "Vittoria", telefono: "+39 879 456 1321" },
    { nome: "Giorgio", telefono: "+39 879 456 3821" },
    { nome: "Andrea", telefono: "+39 859 456 3351" },
  ],

  // Creiamo i metodi (comportamenti) dell'oggetto rubrica

  showContacts: function () {
    // console.log(event);
    // console.log(this);                                        // this non corrisponde più all'oggetto ma corrisponde al button, perché all'interno della callback di addEventListener this corrisponde al button

    let wrapper = document.querySelector("#contacts-wrapper"); //  al button che sto richiamando, o meglio a ciò che si trova sulla sinistra di addEventListener, prima del punto. In quel contesto non ho l'oggetto rubrica, per averlo dovrei dichiarare una nuova funzione di callback function() {rubrica.showContacs}
    // wrapper.innerHTML= "";                                   // cancello il contenuto prima di visualizzarlo in modo tale che non mette in coda ogni volta che premo sul bottone
    this.contatti.forEach(function (contatto, index) {
      // quindi quando non abbiamo gli oggetti giusti cerchiamo di richiamarle le funzioni, evitiamo di passare riferimenti, cerchiamo di richiamarle dall'oggetto che stiamo utilizzando.

      let card = document.createElement("div");
      card.classList.add("my-card");
      card.classList.add("mt-4"); 
      card.classList.add("me-2");
      card.classList.add("position-relative");
      card.id = slugify(contatto.nome); // con replace sostituiamo lo spazio con meno
      card.innerHTML = `
            <button id ="edit-${card.id}" class="btn text-dark position-absolute top-0 end-0" title="Modifica ${contatto.nome}">
            <i class="fa-solid fa-pen-to-square fa-1x"></i>
            </button>   
            <div class="avatar">
            <i class="fa-solid fa-user fa-3x"></i>
            </div>
            <div class="info">
            <span>Nome:</span>
            <span>${contatto.nome}</span>
            </div>
            <div class="info">
            <span>Telefono:</span>
            <span>${contatto.telefono}</span>
            </div>
            
            `;
      wrapper.appendChild(card);
      let buttonEditContact = document.querySelector(`#edit-${card.id}`);
      buttonEditContact.addEventListener("click", function () {
        let hiddenContact = document.querySelector("#hidden-contact");

        let editButton = document.querySelector("#edit-contact");
        let editName = document.querySelector("#modifica-nome");
        let editTel = document.querySelector("#modifica-telefono");
        editName.value = contatto.nome;
        editTel.value = contatto.telefono;

        // riabilito gli input e il button
        editName.removeAttribute("disabled");
        editTel.removeAttribute("disabled");
        editButton.removeAttribute("disabled");

        hiddenContact.value = card.id;

        // let editContact = document.querySelector(`#${contatto.nome}`);
        // wrapper.removeChild(editContact);
        // rubrica.contatti = rubrica.contatti.filter((c) => c.nome != contatto.nome);  // filtrami i valori che non hanno quel nome, inserisco un not equal
        // console.log(rubrica.contatti);
      });
    });
    let showContacts = document.querySelector("#show-contacts"); // tutti gli elementi tipo button, input etc..hanno un attributo disabled che è booleano.
    showContacts.setAttribute("disabled", true);
    let hideContacts = document.querySelector("#hide-contacts");
    hideContacts.removeAttribute("disabled");
    showContatti = true;
  },

  eliminaContatto: function (nome) {
    // let len = this.contatti.length;                                                       // mi salvo al lunghezza del mio array
    let contatto = this.contatti.find(function (contatto) {
      return contatto.nome.toLowerCase() == nome.toLowerCase(); // uso la find per trovarmi un contatto con lo stesso nome che gli passo io dall'input
    });

    if (!contatto) {
      // se non c'è, if not contatto
      throw {
        // lanciami l'errore, l'errore lo lanciamo dentro il metodo eliminaContatto, il catch è all'esterno del metodo
        status: 404, // il catch è nel button associato all'evento. L'errore verrà lanciato all'esterno di questo metodo.
        message: `Contatto ${nome} inesistente!`,
      };
    }

    this.contatti = this.contatti.filter(function (contatto) {
      // funzione scrtta in expression e non come arrow function. Aggiornamento dell'oggetto this
      return contatto.nome.toLowerCase() != nome.toLowerCase(); // filtrami i valori che non hanno quel nome, inserisco un not equal. Filtra se è vera questa condizione
    });

    // if (len == this.contatti.length) {          // se questa condizione è verificata significa che non ho filtrato niente
    //     throw {
    //         status: 404,
    //         message:`Contatto ${nome} inesistente!`
    //     }
    // }

    // Parte grafica
    let wrapper = document.querySelector("#contacts-wrapper");
    let deleteCard = document.querySelector(`#${nome.toLowerCase()}`);
    if (deleteCard) {
      wrapper.removeChild(deleteCard); // rimuovo la card
    }
  },

  hideContacts: function () {
    let wrapper = document.querySelector("#contacts-wrapper");
    wrapper.innerHTML = ``;
    let showContacts = document.querySelector("#show-contacts");
    showContacts.removeAttribute("disabled");
    let hideContacts = document.querySelector("#hide-contacts");
    hideContacts.setAttribute("disabled", true);
    showContatti = false;
  },

  ricercaContatto: function (nome) {
    // questo sotto è il nome passato all'interno della funzione
    const contatto = this.contatti.find(
      (contatto) => contatto.nome.toLowerCase() == nome.toLowerCase()
    ); // li confronto in base al suo minuscolo, lo rendiamo non sensibile al maiuscolo e minuscolo. Controlliamo all'interno di un oggetto una chiave.
    let wrapper = document.querySelector("#search-wrapper");
    if (contatto) {
      let card = document.createElement("div");
      card.classList.add("my-card");
      card.classList.add("mt-4");
      card.classList.add("me-2");
      card.innerHTML = `
            <div class="avatar">
            <i class="fa-solid fa-user fa-3x"></i>
            </div>
            <div class="info">
            <span>Nome:</span>
            <span>${contatto.nome}</span>
            </div>
            <div class="info">
            <span>Telefono:</span>
            <span>${contatto.telefono}</span>
            </div>
            
            `;
      wrapper.appendChild(card);
    } else {
      wrapper.innerHTML = "";
    }
  },

  inserisciContatto: function (nome, telefono) {
    try {
      const contattoNuovo = this.contatti.find(
        (contatto) => contatto.nome.toLowerCase() == nome.toLowerCase()
      );
      if (contattoNuovo)
        throw {
          status: 503,
          message: `Nome ${nome} già esistente!`,
        };

      this.contatti.push({
        nome: nome,
        telefono: telefono,
      });

      if (showContatti) {
        let wrapper = document.querySelector("#contacts-wrapper");
        let card = document.createElement("div");
        card.classList.add("my-card");
        card.classList.add("mt-4"); // classi di bootstrap
        card.classList.add("me-2");
        card.classList.add("position-relative");
        card.id = slugify(nome); // mettiamo il nome come id ma poteva essere qualsiasi cosa
        card.innerHTML = `
                <button id ="edit-${card.id}" class="btn text-dark position-absolute top-0 end-0" title="Modifica ${nome}">
                <i class="fa-solid fa-pen-to-square fa-1x"></i>
                </button>   
                <div class="avatar">
                <i class="fa-solid fa-user fa-3x"></i>
                </div>
                <div class="info">
                <span>Nome:</span>
                <span>${nome}</span>
                </div>
                <div class="info">
                <span>Telefono:</span>
                <span>${telefono}</span>
                </div>
                
                `;
        wrapper.appendChild(card);
        let buttonEditContact = document.querySelector(`#edit-${card.id}`);
        buttonEditContact.addEventListener("click", function () {
          let hiddenContact = document.querySelector("#hidden-contact");

          let editButton = document.querySelector("#edit-contact");
          let editName = document.querySelector("#modifica-nome");
          let editTel = document.querySelector("#modifica-telefono");
          editName.value = nome;
          editTel.value = telefono;

          // riabilito gli input e il button
          editName.removeAttribute("disabled");
          editTel.removeAttribute("disabled");
          editButton.removeAttribute("disabled");

          hiddenContact.value = card.id;
          // let editContact = document.querySelector(`#${contatto.nome}`);
          // wrapper.removeChild(editContact);
          // rubrica.contatti = rubrica.contatti.filter((c) => c.nome != contatto.nome);  // filtrami i valori che non hanno quel nome, inserisco un not equal
          // console.log(rubrica.contatti);
        });
      }
    } catch (error) {
      errorHandling(error);
    }
  },

  modificaContatto: function (nome, telefono, id) {
    // prendiamo l'id della card che abbiamo assegnato nella showContacts(), perché vi serviva un riferimento della card per poter risalire alla card, un identificativo
    try {
      // this.contatti.forEach(function(contatto) {})
      let trovato = false;
      for (let contatto of this.contatti)
        if (contatto.nome.toLowerCase() == id) {
          // se (mentre cicla, contatto.nome == all'id che sta salvato nell'hidden input salvato quando clicchiamo su modifica nella card)
          contatto.nome = nome; // se è così modificami il contatto con i miei nuovi valori
          contatto.telefono = telefono;
          trovato = true;
          break; // inutile ciclare tutta la lista, perché tanto è un elemento che stiamo passando alla funzione
        }

      if (!trovato) {
        // per come è impostato il codice è impossibile che non lo trovi
        throw {
          status: 404,
          message: `Contatto ${nome} inesistente!`,
        };
      }
      this.hideContacts();
      this.showContacts();
    } catch (error) {
      errorHandling(error);
    }
  },
};

// Events - Gestione della GUI

let showContacts = document.querySelector("#show-contacts");
// BRUTTO! showContacts.addEventListener('click', rubrica.showContacts); // questo è un riferimento, è come se non avessi la roba a sinistra del punto, quindi il this sarà per il button e non per l'oggetto rubrica. Sto passando un riferimento senza passare la funzione. Chi esegue la funzione è l'oggetto button showContacts.
showContacts.addEventListener("click", function () {
  // in questo caso è diverso, i concetti sono diversi tra la prima soluzione e la seconda, cambiano gli oggetti.
  rubrica.showContacts();
}); // OTTIMO
showContacts.addEventListener("mouseenter", function () {
  // evento quando il mouse entra nel componente solo per far vedere l'evento, per didattica
  console.log("In visualizza");
});
showContacts.addEventListener("mouseleave", function () {
  // evento quando il mouse esce dal componente solo per far vedere l'evento, per didattica
  console.log("Out visualizza");
});
showContacts.addEventListener("mouseover", function () {
  // evento quando il mouse si trova sopra
  console.log("Inside visualizza");
});

// showContacts.addEventListener('click', pippo); // ACCETTABILE
// function pippo() { rubrica.showContacts()};

let searchContact = document.querySelector("#search-contact");
searchContact.addEventListener("input", function () {
  rubrica.ricercaContatto(searchContact.value);
});

let hideContacts = document.querySelector("#hide-contacts");
hideContacts.addEventListener("click", function () {
  rubrica.hideContacts();
});

// Button elimina contatto
let buttonMainDeleteContacts = document.querySelector("#deleteContact");
buttonMainDeleteContacts.addEventListener("click", function () {
  let deleteSearchContact = document.querySelector("#delete-search-contact");

  if (deleteSearchContact.value) {
    try {
      if (
        deleteSearchContact.value.includes("%") ||
        deleteSearchContact.value.includes("&") ||
        deleteSearchContact.value.includes("$")
      ) {
        // throw "Caratteri non permessi (% & $)";                  // questo sarà il message error che verrà passato al catch come parametro
        // posso anche crearmi un oggeto dell'errore
        throw {
          status: 500,
          message: "Caratteri non permessi (%, &, $)",
        };
      } else {
        rubrica.eliminaContatto(deleteSearchContact.value);
      }
    } catch (error) {
      errorHandling(error);
    } finally {
      deleteSearchContact.value = "";
      console.log(rubrica.contatti);
    }
  }
});

// Button inserisci contatto
let insertContacts = document.querySelector("#insert-contact");
insertContacts.addEventListener("click", function () {
  let nomeInsert = document.querySelector("#nome-insert");
  let telefonoInsert = document.querySelector("#telefono-insert");

  rubrica.inserisciContatto(nomeInsert.value, telefonoInsert.value);
  nomeInsert.value = "";
  telefonoInsert.value = "";
  console.log(rubrica.contatti);
});

// Button modifica contatto
let editContact = document.querySelector("#edit-contact");
editContact.addEventListener("click", function () {
  let hiddenContact = document.querySelector("#hidden-contact");
  let editName = document.querySelector("#modifica-nome");
  let editTel = document.querySelector("#modifica-telefono");
  rubrica.modificaContatto(editName.value, editTel.value, hiddenContact.value); // se invece di rubrica mettessimo this, l'oggetto che passerebbe nella funzione non sarebbe più rubrica ma passerebbe il button edit contact
  editName.value = ""; // questo perché il metodo addEventListener è di button, ma a noi serve l'oggetto globale rubrica
  editTel.value = ""; // noi i comportamenti o li assegniamo tramite function alla funzione dell'evento, lo scriviamo dentro, oppure creaimo il metodo nell'oggetto globale rubrica e poi lo richiamiamo nella funzione dell'addEventListener.
  hiddenContact.value = ""; // oppure potrebbe essere anche una function definita a livello globale come quella dell'errore.
});

// // Altro modo di scrivere il for each della funzione showContacts, con un arrow function

//  this.contatti.forEach((contatto => {
//  let card = document.createElement("div");
//  card.classList.add("my-card");

//  card.innerHTML =

//     `
//     <div class="avatar">
//         <i class="fa-solid fa-user fa-3x"></i>
//     </div>
//     <div class="info">
//         <span>Nome:</span>
//         <span>${contatto.nome}</span>
//     </div>
//     <div class="info">
//         <span>Telefono:</span>
//         <span>${contatto.telefono}</span>
//     </div>

//     `
//     wrapper.appendChild(card);
// })

// funzione di cambio nome per lo spazio, in questo modo possiamo richiamarla dove vogliamo, insertia nello scope globale, quindi una funzione globale
function slugify(nome) {
  return nome.replace(" ", "-").toLowerCase(); // sostituiamo il carattre spazio col carattere meno
}

// funzione di errore, in questo modo possiamo richiamarla dove vogliamo, insertia nello scope globale, quindi una funzione globale
function errorHandling(error) {
  // console.log(error); // error sarebbe l'errore lanciato nel thrown

  // <!-- Toast di errore bootstrap -->
  // var toastTrigger = document.querySelector('#liveToastBtn')
  let errorToast = document.querySelector("#errorToast");
  let errorMessage = document.querySelector("#error-message");
  let errorHeader = document.querySelector("#error-header");

  errorToast.classList.add("toast");
  errorToast.classList.remove("bg-danger");
  errorToast.classList.remove("bg-warning");
  errorToast.classList.remove("bg-primary");
  switch (error.status) {
    case 502: // aggiungiamo più casi con lo switch
    case 501:
    case 500:
      errorToast.classList.add("bg-danger");
      errorHeader.innerHTML = "Errore";
      break;

    case 503:
      errorToast.classList.add("bg-info");
      errorHeader.innerHTML = "Attenzione";

    case 404:
      errorToast.classList.add("bg-warning");
      errorHeader.innerHTML = "Attenzione";
      break;

    default:
      errorToast.classList.add("bg-primary");
      errorHeader.innerHTML = "Info";
      break;
  }

  // if (toastTrigger) {
  //   toastTrigger.addEventListener('click', function () {
  let toast = new bootstrap.Toast(errorToast); // qui generiamo con questo JavaScript che è di bootstrap
  errorMessage.innerHTML = `
    <strong>${error.status}</strong>:
    <span>${error.message}</span>
    
    `;
  toast.show(); // visualizziamo il toast con il metodo show
  //   })
  // }
  setTimeout(function () {
    toast.hide();
    console.log("Ho chiuso il popup");
  }, 3000);
}
